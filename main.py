import kivy

from kivy.app import App
from kivy.uix.label import Label
from kivy.clock import Clock
from kivy.lib import osc
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput

from jnius import autoclass
from android import AndroidService

main_label = Label(font_size='24sp')

activityport = 9000
serviceport = 9001
user = None

def update(message,*args):
	main_label.text = message[2]

class InterfaceManager(BoxLayout):
	def __init__(self, **kwargs):
		global main_label
		super(InterfaceManager, self).__init__(**kwargs)

		self.main_label = main_label

		self.main_button1 = Button(text='Start', font_size='30sp')
		self.main_button1.bind(on_press=self.show2)

		self.main_button2 = Button(text='Stop', font_size='30sp')
		self.main_button2.bind(on_press=self.show1)
		
		self.main_user = Label(font_size='24sp')
		self.main_textinput = TextInput()
		self.service = AndroidService('TrackEEEr','')
		self.show1(self.main_button2)

	def show1(self,button):
		self.service.stop()
		self.clear_widgets()
		self.add_widget(self.main_textinput)
		self.add_widget(self.main_button1)
		self.main_textinput.text = ''

	def show2(self,button):
		self.clear_widgets()
		self.add_widget(self.main_user)
		self.add_widget(self.main_label)
		self.add_widget(self.main_button2)
		self.main_user.text = "User: "+self.main_textinput.text

		main_label.text = ''
		self.service.start()
		Clock.schedule_once(lambda *x: osc.sendMsg('/api', [self.main_textinput.text, ], port=serviceport), 3)

class MainApp(App):
	def build(self):
		osc.init()
		oscid = osc.listen(ipAddr='127.0.0.1', port=activityport)
		osc.bind(oscid, update, '/api')
		Clock.schedule_interval(lambda *x: osc.readQueue(oscid), 0)

		return InterfaceManager(orientation='vertical')

	def on_start(self):
		pass

	def on_stop(self):
		pass

	def on_pause(self):
		return True

	def on_resume(self):
		pass

	def on_destroy(self):
		pass

if __name__ == '__main__':
	MainApp().run()