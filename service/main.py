from kivy.lib import osc

from time import sleep
import socket
import json
from select import select

from jnius import autoclass

Context = autoclass('android.content.Context')
PythonService = autoclass('org.renpy.android.PythonService')

activityport = 9000
serviceport = 9001
clientport = 9002

serverip = '192.168.1.51'#'10.158.22.190' # Josh's CoE151 Server (pls don't DDOS, i will knife ya)
serverport = 20000 # Note to self to setup the firewall for this port
user = None

freq = 10

def send_message(msg):
	osc.sendMsg('/api', [msg,], port=activityport)

def update_user(message, *args):
	global user
	user = message[2]

if __name__ == '__main__':
	osc.init()
	oscid = osc.listen(ipAddr='127.0.0.1', port=serviceport)
	osc.bind(oscid, update_user, '/api')

	send_message("Initializing...")

	##################################################

	service = PythonService.mService
	wifiManager = service.getSystemService(Context.WIFI_SERVICE)

	sck = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
	sck.setblocking(False)
	sck.bind(('',clientport))

	while user==None:
		osc.readQueue(oscid)

	##################################################

	send_message("Initialization Complete!")

	sleep(3)
	counter = 0
	while True:
		osc.readQueue(oscid)

		if counter==2:
			wifiManager.startScan()

		if counter==0:
			scan_result_list = wifiManager.getScanResults()
			send_message('Broadcasting!')

			ssids = []
			rssis = []
			for i in range(scan_result_list.size()):
				scan_result = scan_result_list.get(i)
				ssids.append(scan_result.SSID)
				rssis.append(scan_result.level)
			out = json.dumps({"id":user, "ssids":ssids, "rssis":rssis},indent=4)+'\n'

			_,write_list,_ = select([],[sck],[])
			if write_list:
				sck.sendto(out,(serverip,serverport))
		else:
			send_message(('%ds until next broadcast' % counter))
		counter = (counter+freq-1)%freq
		sleep(1)